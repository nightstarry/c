#include "LinkedHashTable.h"

const int SIZE = 5;

Element::Element(int data, int key, Element* next)
{
	this->data = data;
	this->next = next;
	this->key = key;
}

LinkedHashTable::LinkedHashTable()
{
	countElem = 0;
	size = SIZE; 
	hashTable = new Element*[size];
	list = new List();
	for (int i = 0; i < size; i++) {
		hashTable[i] = nullptr;
	}
}

LinkedHashTable::LinkedHashTable(int size)
{
	countElem = 0;
	this->size = size;
	hashTable = new Element*[size];
	list = new List();
	for (int i = 0; i < size; i++) {
		hashTable[i] = nullptr;
	}
}

void LinkedHashTable::addElem(int elem, int key)
{

	int position = key % size;
	if (!hashTable[position]) {
		hashTable[position] = new Element(elem, key);
	}
	else {
		Element* temp = hashTable[position];
		while (temp->next) {
			temp = temp->next;
		}
		temp->next = new Element(elem, key);
	}
	list->addElem(elem, key);
	countElem++;
}

void LinkedHashTable::removeElem(int key)
{
	int position = key % size;
	Element* temp = hashTable[position];
	Element *buf = temp;
	while (temp) {
		if (key == temp->key) {
			if (buf == temp) {
				hashTable[position] = temp->next;
			}
			else {
				buf->next = temp->next;
			}
			list->delElem(temp->data);
			delete temp;
			countElem--;
			return;
		}
		buf = temp;
		temp = temp->next;
	}
}

int LinkedHashTable::findElem(int key)
{
	int position = key % size;
	Element* temp = hashTable[position];
	while (temp) {
		if (key == temp->key) {
			return temp->data;
		}
		temp = temp->next;
	}
	throw NoElemException();
}

void LinkedHashTable::doEmpty()
{
	for (int i = 0; i < size; i++) {
		Element *temp = hashTable[i];
		Element *buf;
		while (temp) {
			buf = temp->next;
			delete temp;
			temp = buf;
		}
		hashTable[i] = nullptr;
	}
	countElem = 0;
	list->doEmpty();
}

bool LinkedHashTable::isEmpty()
{
	if (countElem > 0) { return false; }
	return true;
}

void LinkedHashTable::printElem()
{
	for (int i = 0; i < size; i++) {
		Element* temp = hashTable[i];
		cout << i << ": ";
		while (temp) {
			cout << " " << temp->data << " ";
			temp = temp->next;
		}
		cout << endl;
	}
}


LinkedHashTable::~LinkedHashTable()
{
	doEmpty();
	delete[] hashTable;
	delete list;
}

List::List()
{
	first = nullptr;
	last = nullptr;
}

List::~List()
{
	Element* temp = first;
	while (temp != last) {
		temp = temp->next;
		delete first;
		first = temp;
	}
	delete first;
}

void List::addElem(int elem, int key)
{
	if (!last) {
		last = new Element(elem, key);
		first = last;
	} 
	else {
		last->next = new Element(elem, key);
		last = last->next;
	}
}

void List::delElem(int elem)
{
	Element* temp = first->next;
	Element* buf = first;
	if (first->data == elem) {
		delete first;
		first = temp;
		return;
	}
	while (temp->data != elem) {
		buf = temp;
		temp = temp->next;
	}
	buf->next = temp->next;
	delete temp;

}

void List::doEmpty()
{
	Element* temp = first;
	while (temp != last) {
		temp = temp->next;
		delete first;
		first = temp;
	}
	delete first;
}

Iterator::Iterator(LinkedHashTable* & hashTable)
{
	hash = hashTable;
	now = hash->list->first;
}

Iterator::~Iterator()
{
}

bool Iterator::hasNext()
{
	if (now != hash->list->last) { return true; }
	return false;
}

void Iterator::start()
{
	now = hash->list->first;
}

void Iterator::next()
{
	now = now->next;
}

int Iterator::seeElem()
{
	return now->data;
}

IteratorTable::IteratorTable(LinkedHashTable*  hashTab)
{
	hash = hashTab;
	if (hash->isEmpty()) {
		throw IteratorClException();
	}
	start();
}

IteratorTable::~IteratorTable()
{
	count = 0;
	hash = nullptr;
	now = nullptr;
}

bool IteratorTable::finish()
{
	return now == nullptr;
}

void IteratorTable::start()
{
	for (count = 0; hash->hashTable[count] == nullptr; count++);
	now = hash->hashTable[count];
}

void IteratorTable::next()
{
	if (now->next != nullptr) {
		now = now->next;
	}
	else {
		for (count++; count < hash->size && hash->hashTable[count] == nullptr; count++);
		if (count == hash->size) {
			now = nullptr;
		}
		else {
			now = hash->hashTable[count];
		}
	}
}

int IteratorTable::seeElem()
{
 	return now->data;
}