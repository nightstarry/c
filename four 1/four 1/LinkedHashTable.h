#include "iostream"
#include "locale.h"
#include "fstream"

using namespace std;

struct NoElemException {};
struct IteratorClException {};

struct Element
{
	int data;
	int key;
	Element* next;
	Element(int data, int key, Element* next = nullptr);
};

class List {
private:
	Element* first;
	Element* last;
public:
	friend class Iterator;
	List();
	~List();
	void addElem(int elem, int key);
	void delElem(int elem);
	void doEmpty();
};

class LinkedHashTable
{
private:
	Element** hashTable;
	List* list;
	int size;
	int countElem;
public:
	friend class Iterator;
	friend class IteratorTable;
	LinkedHashTable();
	LinkedHashTable(int size);
	void addElem(int elem, int key);
	void removeElem(int key);
	int findElem(int key);
	void doEmpty();
	bool isEmpty();
	void printElem();
	~LinkedHashTable();  
};


class Iterator {
private:
	Element* now;
	LinkedHashTable* hash;

public:
	Iterator(LinkedHashTable* &hashTable);
	~Iterator();
	bool hasNext();
	void start();
	void next();
	int seeElem();
};


class IteratorTable {
private:
	Element* now;
	LinkedHashTable* hash;
	int count;

public:
	IteratorTable(LinkedHashTable* hashTable);
	~IteratorTable();
	bool finish();
	void start();
	void next();
	int seeElem();
};