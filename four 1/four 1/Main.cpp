#include "LinkedHashTable.h"


int main()
{
	setlocale(LC_ALL, "Rus");

	LinkedHashTable* oneHT = new LinkedHashTable();

	oneHT->addElem(1, 0);
	oneHT->addElem(2, 3);
	oneHT->addElem(3, 2);
	oneHT->addElem(4, 5);
	oneHT->addElem(15, 1);
	oneHT->addElem(5, 2);
	oneHT->addElem(5, 4);

	// 1, 4, 15, 3, 5, 2, 5

	Iterator itOne(oneHT);

	//oneHT->printElem();
	//cout << endl;

	IteratorTable itOne1(oneHT);

	itOne1.start();
	while (!itOne1.finish()) {
		cout << itOne1.seeElem() << endl;
		itOne1.next();
	}
	cout << endl;
	cout << endl;
	cout << endl;

	itOne.start();
	while (itOne.hasNext()) {
		cout << itOne.seeElem() << endl;
		itOne.next();
	}
	cout << itOne.seeElem() <<  endl;

	cout << endl;	
	
	cout <<"������� ������ �������� �� ����� 4" <<endl;
	oneHT->removeElem(4);

	itOne.start();
	while (itOne.hasNext()) {
		cout << itOne.seeElem() << endl;
		itOne.next();
	}

	cout << itOne.seeElem() << endl;

	cout << endl;

	try { 
		cout << "����� ������ �������� �� ����� 2" << endl;
		cout << oneHT->findElem(2) << endl; 
	}
	catch (NoElemException e) {
		cout << "��� ������ ��������!" << endl;
	}
	
	try {
		cout << "����� ������ �������� �� ����� 3" << endl;
		cout << oneHT->findElem(3) << endl;
	}
	catch(NoElemException e){
		cout << "��� ������ ��������!" << endl;
	}

	cout << endl;

	cout << "isEmpty: " << oneHT->isEmpty() << endl;
	oneHT->doEmpty();
	cout << "isEmpty: " << oneHT->isEmpty() << endl;

	system("pause");

	return 0;
}

