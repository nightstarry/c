#include "TreeDictionary.h"


Node::Node(string word, int occurrence,  Node* right, Node* left)
{
	this->word = word;
	this->occurrence = occurrence;
	this->right = right;
	this->left = left;
}

TreeDictionary::TreeDictionary()
{
	root = nullptr;
	size = 0;
}

void TreeDictionary::copyTree(Node* root, Node* copy)
{
	if (!copy) {
		return;
	}
	root = new Node(copy->word, copy->occurrence);
	copyTree(root->left, copy->left);
	copyTree(root->right, copy->right);
}

TreeDictionary::TreeDictionary(const TreeDictionary & copy)
{
	copyTree(root, copy.root);
	size = copy.size;
}

TreeDictionary::TreeDictionary(TreeDictionary && copy)
{
	size = copy.size;
	swap(root, copy.root);
}

//�������� ������������
TreeDictionary& TreeDictionary::operator=(const TreeDictionary& copy)
{ 
	if (this == &copy) {
		return* this;
	}
	deleteTree(root);
	root = nullptr;
	size = copy.size;
	copyTree(root, copy.root);
}

//�������� �����������
TreeDictionary& TreeDictionary::operator=(TreeDictionary&& copy)
{
	if (this == &copy) {
		return* this;
	}
	size = copy.size;
	swap(root, copy.root);
	copy.deleteTree(copy.root);
	return* this;
}

void TreeDictionary::deleteTree(Node * root)
{
	if (!root) {
		return;
	}
	deleteTree(root->left);
	deleteTree(root->right);
	delete root;
}

TreeDictionary::~TreeDictionary()
{
	size = 0;
	deleteTree(root);
}

//����� �����, ��������� ���������� ���������
int TreeDictionary::findWord(Node* root, string word)
{
	if (!root) { return 0; }
	if (root->word.compare(word) == 0) {
		return root->occurrence;
	}
	if (root->word.compare(word) > 0) {
		return findWord(root->left, word);
	}
	if (root->word.compare(word) < 0) {
		return findWord(root->right, word);
	}
}

int TreeDictionary::findWord(string word)
{
	return findWord(root, word);
}

void TreeDictionary::addWord(Node *& root, string word)
{
	if (!root) {
		root = new Node(word);
		return;
	}
	if (root->word.compare(word) == 0) {
		root->occurrence++;
		return;
	}
	if (root->word.compare(word) > 0) {
		addWord(root->left, word);
	}
	if (root->word.compare(word) < 0) {
		addWord(root->right, word);
	}
}

void TreeDictionary::addWord(string word)
{
	size++;
	addWord(root, word);
}

void TreeDictionary::del2(Node*& rootLeft, Node*& toDel)
{
	if (rootLeft->right) {
		del2(rootLeft->right, toDel);
	}
	toDel->word = rootLeft->word;
	toDel->occurrence = rootLeft->occurrence; 
	toDel = rootLeft;
	rootLeft = rootLeft->left;
}

void TreeDictionary::deleteWord(Node*& root, string& word)
{
	if (!root) {
		return;
	}
	if (root->word.compare(word) > 0) {
		deleteWord(root->left, word);
		return;
	}
	if (root->word.compare(word) < 0) {
		deleteWord(root->right, word);
		return;
	}
	Node* pDel = root;
	if (root->occurrence > 1) {
		root->occurrence--;
	}
	else {
		if (!root->right) {
			root = root->left;
			delete pDel;
			return;
		}
		if (!root->left) {
			root = root->right;
			delete pDel;
			return;
		}
		del2(root->left, pDel);
		delete pDel;
	}
}

void TreeDictionary::deleteWord(string word)
{
	size--;
	deleteWord(root, word);
}
int TreeDictionary::countWords()
{
	return size;
}

void TreeDictionary::printTree(ostream& out, Node* root, int ic)
{
	if (!root) {
		out << endl;
		return;
	}
	for (int i = 0; i < ic; i++) {
		out << "\t";
	}
	out << root->word << endl;
	printTree(out, root->left, ic + 1);
	printTree(out, root->right, ic + 1);
}

//����� ������ � ���������� ������� � ��������
void TreeDictionary::printTree1(ostream& out, Node* root)
{
	if (!root) { return; }
	printTree1(out, root->left);
	out << root->word << " - " << root->occurrence << endl;
	printTree1(out, root->right);
}

std::ostream& operator<< (ostream& out, TreeDictionary& obj)
{
	obj.printTree1(out, obj.root);
	return out;
}