#include "iostream"
#include "string"
#include <locale.h>
#include "fstream"

using namespace std;

struct Node { 
	string word;
	int occurrence;
	Node *right, *left;
	Node(string word, int occurrence = 1, Node *right = nullptr, Node *left = nullptr);
};

class TreeDictionary
{
private:
	Node* root;
	int size;
	void del2(Node *&rootLeft, Node *&toDel);
	void copyTree(Node *root, Node *copy);
	void deleteTree(Node *root);
	int findWord(Node *root, string word);
	void addWord(Node *&root, string word);
	void deleteWord(Node *&root, string &word);
	void printTree(ostream &os, Node *root, int ic);
	void printTree1(ostream &os, Node *root);
public:
	TreeDictionary();
	TreeDictionary(const TreeDictionary &copy);
	TreeDictionary(TreeDictionary &&move);
	~TreeDictionary();
	TreeDictionary& operator=(const TreeDictionary &copy);
	TreeDictionary& operator=(TreeDictionary &&move);
	int findWord(string word);
	void addWord(string word);
	void deleteWord(string word);
	int countWords();
	friend ostream& operator<< (ostream& o��, TreeDictionary& obj);
};
