#include "BinarTree.h"


TreeElem::TreeElem(int data, TreeElem* left, TreeElem* rigth)
{
	this->data = data;
	this->left = left;
	this->right = rigth;
}

BinarTree::BinarTree()
{
	root = nullptr;
	size = 0;
}

void BinarTree::copyTree(TreeElem*& root, TreeElem* copy)
{
	if (!copy) { return; }
	root = new TreeElem(copy->data, nullptr, nullptr);
	copyTree(root->left, copy->left);
	copyTree(root->right, copy->right);
}

BinarTree::BinarTree(const BinarTree &copy)
{
	copyTree(root, copy.root);
	size = copy.size;
}

BinarTree::BinarTree(BinarTree &&copy)
{
	swap(root, copy.root);
	size = copy.size;
}

void BinarTree::removeTree(TreeElem* root)
{
	if (!root) { return; }
	removeTree(root->left);
	removeTree(root->right);
	if ((!root->left) && (!root->right)) {
		delete root;
	}
}

//�������� ������������
BinarTree &BinarTree::operator=(const BinarTree &copy)
{
	if (&copy == this) {
		return *this;
	}
	removeTree(root);
	root = nullptr;
	copyTree(root, copy.root);
	size = copy.size;
	return *this;
}

//�������� �����������
BinarTree &BinarTree::operator=(BinarTree &&copy)
{
	if (&copy == this) {
		return *this;
	}
	removeTree(root);
	swap(root, copy.root);
	size = copy.size;
	return *this;
}

BinarTree::~BinarTree()
{
	removeTree(root);
	root = nullptr;

}

//���������� ������ �����
int BinarTree::countEven(TreeElem* root)
{
	if (!root) { return 0; }
	if (root->data % 2 == 0) { return 1 + countEven(root->left) + countEven(root->right); }
	return countEven(root->left) + countEven(root->right);
}

int BinarTree::countEven()
{
	return countEven(root);
}

//�������� ��� ������������� ��������
bool BinarTree::positiveElem(TreeElem* root)
{
	if (!root) { return true; }
	if (root->data >= 0) { return positiveElem(root->left) && positiveElem(root->right); }
	return false;
}

bool BinarTree::positiveElem()
{
	return positiveElem(root);
}

void BinarTree::removeLeaves(TreeElem*& root)
{
	if (!root) { return; }
	if (!root->left && !root->right) 
	{
		delete root;
		root = nullptr;
	}
	else 
	{
		removeLeaves(root->left);
		removeLeaves(root->right);
	}
}

void BinarTree::removeLeaves()
{
	removeLeaves(root);
}

//����� ���� ���������
int BinarTree::getSum(TreeElem* root)
{
	if (!root) { return 0; }
	return (root->data + getSum(root->left) + getSum(root->right)) ;
}

// ������� ��������������
double BinarTree::middle()
{
	return getSum(root) / size;
}

bool BinarTree::findElem(int x, TreeElem* root, vector<int> &vect)
{
	if (!root) { return false; }
	if (root->data == x) { return true; }
	vect.push_back(0); 
	if (findElem(x, root->left, vect)) {
		return true;
	}
	vect.pop_back(); 
	vect.push_back(1); 
	if (findElem(x, root->right, vect)) {
		return true;
	}
	vect.pop_back();
	return false;
}

vector<int> BinarTree::findElem(int x)
{
	vector<int> res = {};
	if (findElem(x, root, res)) {
		return res;
	}
	throw NoElemException();
}


//�������� �������
void BinarTree::addElem(int elem, vector<int> vector)
{
	TreeElem* node = root;

	if (vector.size() == 0) {
		if (root) {
			root->data = elem;
		}
		else {
			root = new TreeElem(elem, nullptr, nullptr);
			size++;
		}
	}
	else {
		for (int i = 0; i < vector.size() - 1; i++) {
			if (!node) { throw VectorException(); } 
			if (vector[i] == 0) {
				node = node->left;
			}
			if (vector[i] == 1) {
				node = node->right; 
			}
		}
		if (vector.back() == 0) {
			if (node->left) {
				node->left->data = elem;
			}
			else {
				node->left = new TreeElem(elem, nullptr, nullptr);
				size++;
			}
		}
		else {
			if (node->right) {
				node->right->data = elem;
			}
			else {
				node->right = new TreeElem(elem, nullptr, nullptr);
				size++;
			}
		}
	}
}


void BinarTree::writeTree(ostream& out, TreeElem* root, int step)
{
	if (!root) { out << endl;         return; }
	for (int i = 0; i < step; i++) {
		out << "\t";
	}
	out << root->data << "\n";
	
	writeTree(out, root->left, step + 1);
	writeTree(out, root->right, step + 1);
}

ostream & operator<<(ostream& out, BinarTree& tree)
{
	tree.writeTree(out, tree.root, 0);
	return out;
}
