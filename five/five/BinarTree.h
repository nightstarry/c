#include <vector>
#include <iostream>
#include <locale.h>
#include "fstream"

using namespace std;

struct VectorException {};
struct NoElemException {};

struct TreeElem {
	int data;
	TreeElem* left;
	TreeElem* right;
	TreeElem(int data, TreeElem* left, TreeElem* rigth);
};

class BinarTree {
private:
	TreeElem* root;
	int size;
	void copyTree(TreeElem*& root, TreeElem* copy);
	void removeTree(TreeElem* root);
	int countEven(TreeElem* root);
	bool positiveElem(TreeElem* root);
	void removeLeaves(TreeElem*& root);
	bool findElem(int x, TreeElem* root, vector<int> &vect);
	int getSum(TreeElem* root);
	void writeTree(ostream& out, TreeElem* root, int step);

public:
	BinarTree();
	BinarTree(const BinarTree &copy);
	BinarTree(BinarTree &&copy);
	BinarTree& operator= (const BinarTree &copy);
	BinarTree& operator= (BinarTree &&copy);
	~BinarTree();
	void addElem(int elem, vector<int> vector);
	int countEven();
	bool positiveElem();
	void removeLeaves();
	double middle();
	vector<int> findElem(int x);

	friend ostream& operator<<(ostream& out, BinarTree &tree);
};


