#include "BinarTree.h"



int main()
{
	setlocale(LC_ALL, "Rus");

	BinarTree *treeOne = new BinarTree();
	
	treeOne->addElem(50, {});
	//cout << *treeOne << endl << endl;

	//treeOne->removeLeaves();
	//cout << *treeOne << endl << endl;
	//treeOne->addElem(51, {});
	//treeOne->addElem(70, { 1 });
	//cout << *treeOne << endl << endl;
	treeOne->addElem(20, { 0 });
	treeOne->addElem(70, { 1 });
	treeOne->addElem(10, { 0, 0 });
	treeOne->addElem(33, { 0, 1 });
	treeOne->addElem(60, { 1, 0 });
	treeOne->addElem(100, { 1, 1 });
	

	cout << *treeOne << endl<< endl;
	
	cout << "������ �����: " << treeOne->countEven() << endl;
	cout << "��������, ��� ��� �������� �������������: " << treeOne->positiveElem() << endl;
	cout << "������� ��������������: " << treeOne->middle() << endl;

	int a = 33;
	vector<int> vec = treeOne->findElem(a);
	cout << "���� �� ����� "<< a << "(����� �������): ";
	for (unsigned int i = 0; i < vec.size(); i++) {
		cout << vec[i] << " ";
	}
	cout << endl << endl;

	BinarTree *treeTwo = new BinarTree(*treeOne);
	treeOne->removeLeaves();
	cout << "�������� �������" << endl;
	cout << *treeOne << endl;
	cout << "_______________________________" << endl << endl;
	cout << *treeTwo << endl << endl;
	cout << "_______________________________" << endl << endl;

	treeOne->addElem(-100, { 1, 1 });
	treeOne->addElem(-60, { 1, 0 });
	cout << *treeOne << endl;
	cout << "��������, ��� ��� �������� �������������: " << treeOne->positiveElem() << endl;

	treeOne->~BinarTree();
	treeTwo->~BinarTree();
	
	system("pause");
	return 0;
}

