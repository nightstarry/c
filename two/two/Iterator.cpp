#include "Iterator.h"


Iterator::Iterator(Queue *&queue)
{
	this->index = queue->head;
	this->queue = queue;
}

Iterator::~Iterator() {
}

void Iterator::start() {
	index = queue->head;
}

void Iterator::next() {
	if (this->finish()) {
		throw IteratorException("Все проитерировано!");
	}
	index = (index + 1) % queue->size;
}

bool Iterator::finish() {
	return (index == (queue->tail % queue->size));
}

int Iterator::getValue() {
	return queue->queue[index];
}