#include "Queue.h"


struct IteratorException {
	string message;
	IteratorException(const string message) { this->message = message; }
};

class Iterator
{
private: Queue *queue;
		 int index;
public:
	Iterator(Queue *&queue);
	~Iterator();
	void start();
	void next();
	bool finish();
	int getValue();
};