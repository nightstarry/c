#include "Box.h"


BoxException::BoxException(const string theMessage) { message = theMessage; }
Box::Box() {}

Box::Box(int theLength, int theWidth, int theHeight, double theWeight, int theValue)
{
	if (theLength <= 0 || theWidth <= 0 || theHeight <= 0 || theWeight <= 0 || theValue <= 0) { throw BoxException("������������ ����!"); }
	length = theLength;
	width = theWidth;
	height = theHeight;
	weight = theWeight;
	value = theValue;
}

int Sum(Box *arr, int size)
{
	int sum = 0;
	for (int i = 0; i < size; i++)
	{
		sum += arr[i].value;
	}
	return sum;
}

bool prov(Box *arr, int size, int leng, int wid, int heig)
{

	bool ok1 = true;
	bool ok2 = true;
	bool ok3 = true;

	int sum1 = 0;
	for (int i = 0; i < size; i++)
	{
		sum1 += arr[i].length;
	}

	if (leng < sum1)
	{
		ok1 = false;
	}

	int sum2 = 0;
	for (int i = 0; i < size; i++)
	{
		sum2 += arr[i].width;
	}

	if (wid < sum2)
	{
		ok2 = false;
	}

	int sum3 = 0;
	for (int i = 0; i < size; i++)
	{
		sum3 += arr[i].height;
	}

	if (heig < sum3)
	{
		ok3 = false;
	}

	if (ok1 == true && ok2 == true && ok3 == true)
	{
		return true;
	}
	return false;
}

double maxWeight(Box arr[M], int size, double maxV)
{
	double max = 0;
	for (int i = 0; i < size; i++)
	{
		if (maxV >= arr[i].weight)
		{
			if (max < arr[i].weight) {
				max = arr[i].weight;
			}
		}

	}
	return max;
}

bool Box::operator <(const Box &box)
{
	if (this->length < box.length && this->width < box.width && this->height < box.height && this->weight < box.weight && this->value < box.value)
	{
		return true;
	}
	return false;
}

bool prov1(Box arr[M], int size)
{
	Box tmp(1, 1, 1, 1, 1);
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (arr[j] < arr[j + 1])
			{
				tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
	for (int i = 0; i < size - 1; i++)
	{
		if (arr[i].length <= arr[i + 1].length || arr[i].width <= arr[i + 1].width || arr[i].height <= arr[i + 1].height)
		{
			return false;
		}
	}
	return true;
}

bool Box:: operator ==(const Box &box)
{
	return this->length == box.length && this->width == box.width && this->height == box.height &&
		this->weight == box.weight && this->value == box.value;
}
istream& operator >>(ifstream &in, Box &one)
{
	in >> one.length >> one.width >> one.height >> one.weight >> one.value;
	return in;
}

ostream& operator <<(ofstream &out, const Box &box)
{
	out << '[' << box.length << ',' << box.width << ',' << box.height << ',' << box.weight << ',' << box.value << ']' << endl;
	return out;
}
