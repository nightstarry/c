#include <exception>
#include <fstream>
#include <iomanip>
#include <clocale>

using namespace std;

const int M = 100;

struct BoxException {
	string message;
	BoxException(const string theMessage);
};
struct Box
{
	int length; // ��
	int width;
	int height;
	double weight; // ����� � ��
	int value; // ��������� ����������� � ��������

	Box();
	Box(int theLength, int theWidth, int theHeight, double theWeight, int theValue);
	friend istream& operator >>(ifstream &in, Box &box);
	friend ostream& operator <<(ofstream &out, const Box &box);
	bool operator ==(const Box &box);
	bool operator <(const Box &box);
};

int Sum(Box *arr, int size);
bool prov(Box arr[M], int size, int leng, int wid, int heig);
double maxWeight(Box arr[M], int size, double maxV);
bool prov1(Box arr[M], int size);

