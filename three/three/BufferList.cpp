#include "BufferList.h"


BufferList::BufferList()
{
	size = 0;
	head = new Node(0);
	head->next = head;
	head->prev = head;
}

BufferList::BufferList(const BufferList & copy)
{
	size = copy.size;
	head = new Node(0);
	head->next = head;
	head->prev = head;
	Node* buf = copy.head->next;

	for (int i = 0; i < size; i++) {
		Node *temp = new Node(0);
		temp->data = buf->data;
		temp->next = head->next;
		temp->prev = head;
		head->next = temp;
		head = temp;
		buf = buf->next;
	}

	head->next->prev = head;
	head = head->next;
}

BufferList::BufferList(BufferList && copy)
{
	size = copy.size;
	swap(head, copy.head);
}

BufferList & BufferList::operator=(const BufferList & copy)
{
	if (this == &copy) {
		return *this;
	}

	makeEmpty();

	Node *buf = copy.head->next;

	for (int i = 0; i < this->size; i++) {
		Node *temp = new Node(0);
		temp->data = buf->data;
		temp->next = head->next;
		temp->prev = head;
		head->next = temp;
		head = temp;
		buf = buf->next;
	}

	head->next->prev = head;
	head = head->next;
	return *this;
}

BufferList & BufferList::operator=(BufferList && copy)
{
	if (this == &copy)
	{
		return *this;

	}
	makeEmpty();
	size = copy.size;
	swap(head, copy.head);
	copy.head = nullptr;
	return *this;
}


BufferList::~BufferList()
{
	makeEmpty();
	delete head;
}

BufferList::ListIterator::ListIterator(BufferList * list)
{
	this->list = list;
	this->now = list->head;
}

void BufferList::ListIterator::start()
{
	now = list->head->next;
}

TElem BufferList::ListIterator::getElement() const
{
	if (now == list->head) {
		throw BufferException();
	}
	return now->data;
}

void BufferList::ListIterator::next()
{
	now = now->next;
}

void BufferList::ListIterator::prev()
{
	now = now->prev;
}

bool BufferList::ListIterator::finish() const
{
	if (now->next == list->head->next) {
		return true;
	}
	return false;
}

Node * BufferList::ListIterator::getNow() const
{
	return  now;
}


void BufferList::addElem(const TElem & elem, Iterator & iter)
{
	size++;
	Node* temp = new Node(elem, iter.getNow()->next, iter.getNow());
	iter.getNow()->next->prev = temp;
	iter.getNow()->next = temp;
}

void BufferList::deleteElem(Iterator & iter)
{
	if (iter.getNow() == head) {
		throw BufferException();
	}
	
	Node* delElem = iter.getNow();
	delElem->next->prev = delElem->prev;
	delElem->prev->next = delElem->next;
	delete delElem;
	size--;
}

void BufferList::makeEmpty()
{
	head = head->next;
	while (size > 0) {
		Node* temp = head->next;
		delete head;
		head = temp;
		size--;
	}
	head->next = head;
	head->prev = head;
	size = 0;
}

bool BufferList::isEmpty() const
{
	if (size == 0) {
		return true;
	}
	return false;
}

int BufferList::getSize() const
{
	return size;
}

Iterator* BufferList::findElem(const TElem & elem)
{
	ListIterator* listIterator = new ListIterator(this);

	listIterator->next();
	while (listIterator->getElement() != elem && !listIterator->finish()) {

		listIterator->next();
	}
	if (listIterator->getElement() != elem) {
		throw NoElemException();
	}
	return listIterator;  
}

Iterator * BufferList::getIterator()
{
	return new ListIterator(this);
}
