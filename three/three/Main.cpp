#include "BufferList.h"

int main()
{
	setlocale(LC_ALL, "Rus");

	int a;

	ifstream in("1.txt");
	ofstream out("2.txt");

	BufferList * bl = new BufferList();
	BufferList::ListIterator iter(bl);

	
	in >> a;
	bl->addElem(a, iter);
	iter.next();
	in >> a;
	bl->addElem(a, iter);
	iter.next();
	in >> a;
	bl->addElem(a, iter);
	


	iter.start();

	while (!iter.finish()) {
		
		try {
			out << iter.getElement() << endl;
		}
		catch (BufferException e) {
			out << " BufferException " << endl;
		}
		iter.next();
	}
	
	Iterator* iter3 = bl->getIterator();
	iter3->next();

	in >> a;

	try
	{
		Iterator *it = bl->findElem(a);
		out << "����� �������: " ;
		out << it->getElement() <<endl;
	}
	catch (NoElemException e)
	{
		out << "������ �������� ���!" << endl;
	}
	catch (BufferException e) {
		out << " BufferException " << endl;
	}

	try
	{
		bl->deleteElem(*iter3);
	}
	catch (BufferException e)
	{
		out << " BufferException " << endl;
	}

	iter.start();

	while (!iter.finish())
	{
		
		try{ out << iter.getElement() << endl;
		}
		catch (BufferException e) { out << " BufferException " << endl;
		}
		iter.next();
	}

	out << "bl Size: " << bl->getSize() << endl;
	out << "bl isEmpty: " << bl->isEmpty() << endl;

	BufferList * bl1 = new BufferList(*bl);
	BufferList::ListIterator iter4(bl1);

	out << "bl1 Size: " << bl1->getSize() << endl;
	out << "bl1 isEmpty: " << bl1->isEmpty() << endl;
	iter4.start();
	while (!iter4.finish())
	{
		
		try {
			out << iter4.getElement() << endl;
		}
		catch (BufferException e) {
			out << " BufferException " << endl;
		}
		iter4.next();
	}

	bl->makeEmpty();

	out << "bl isEmpty: " << bl->isEmpty() << endl;

	BufferList * bl2 = bl1;
	BufferList::ListIterator iter5(bl2);

	out << "bl2 Size: " << bl2->getSize() << endl;
	out << "bl2 isEmpty: " << bl2->isEmpty() << endl;

	iter5.start();
	while (!iter5.finish())
	{
		
		try {
			out << iter5.getElement() << endl;
		}
		catch (BufferException e) {
			out << " BufferException "<< endl;
		}
		iter5.next();
	}

	return 0;
}

